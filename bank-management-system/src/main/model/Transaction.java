package src.main.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Transaction implements Comparable<Transaction> {
    public enum Type {WITHDRAW, DEPOSIT};
    private Type type;
    private long timestamp;
    private String id;
    private double amount;

    public Transaction(Type type, long timestamp, String id, 
            double amount) throws IllegalArgumentException {
        this.setType(type);
        this.setTimestamp(timestamp);
        this.setId(id);
        this.setAmount(amount);
    }

    public Transaction(Transaction source) throws IllegalArgumentException {
        this.setType(source.getType());
        this.setTimestamp(source.getTimestamp());
        this.setId(source.getId());
        this.setAmount(source.getAmount());
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) throws IllegalArgumentException {
        if (id.isEmpty() || id.isBlank()) {
            throw new IllegalArgumentException("Transaction id cannot be null or blank.");
        }
        this.id = id;
    }

    public double getAmount() {
        return this.amount;
    }

    public void setAmount(double amount) throws IllegalArgumentException {
        if (amount < 0) {
            throw new IllegalArgumentException("Transaction amount cannot be negative.");
        }
        this.amount = amount;
    }

    public String returnDate() {
        Date date = new Date(getTimestamp() * 1000);
        return (new SimpleDateFormat("dd-MM-yyyy").format(date));
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Transaction)) {
            return false;
        }
        Transaction transaction = (Transaction) o;
        return Objects.equals(type, transaction.type) 
            && timestamp == transaction.timestamp 
            && Objects.equals(id, transaction.id) 
            && amount == transaction.amount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, timestamp, id, amount);
    }

    @Override
    public int compareTo(Transaction o) {
        return Double.compare(this.getTimestamp(), o.getTimestamp());
    }

    @Override
    public String toString() {
        return this.getType() + "    " +
            "\t" + this.returnDate() + "" +
            "\t" + this.getId() + "" +
            "\t$" + this.amount + "";
    }
}
