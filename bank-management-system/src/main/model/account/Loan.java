package src.main.model.account;

public class Loan extends Account {
    private static final double WITHDRAWAL_LIMIT = 10000.;
    private static final double INTEREST_RATE = 0.02;

    public Loan(String id, String name, double balance) {
        super(id, name, balance);
    }

    public Loan(Loan source) {
        super(source);
    }

    public static double getWithdrawalLimit() {
        return WITHDRAWAL_LIMIT;
    }

    public static double getInterestRate() {
        return INTEREST_RATE;
    }

    public void deposit(double amount) {
        super.setBalance(super.round(super.getBalance() - amount));
    }

    public boolean withdraw(double amount) {
        if (super.getBalance() + amount < WITHDRAWAL_LIMIT) {
            super.setBalance(super.round(super.getBalance() + amount * (1 + INTEREST_RATE)));
            return true;
        }
        return false;
    }

    @Override
    public Account clone() {
        return new Loan(this);
    }
}
