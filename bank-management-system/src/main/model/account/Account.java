package src.main.model.account;

import java.text.DecimalFormat;

public abstract class Account {
    private String id;
    private String name;
    private double balance;

    public Account(String id, String name, double balance) {
        this.setId(id);
        this.setName(name);
        this.setBalance(balance);
    }

    public Account(Account source) {
        this.setId(source.getId());
        this.setName(source.getName());
        this.setBalance(source.getBalance());
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) throws IllegalArgumentException {
        if (id.isEmpty() || id.isBlank()) {
            throw new IllegalArgumentException("Account id cannot be blank or null.");
        } else if (id.length() != 36) {
            throw new IllegalArgumentException("Account id must be of length 36.");
        }
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) throws IllegalArgumentException {
        if (name.isEmpty() || name.isBlank()) {
            throw new IllegalArgumentException("Account name cannot be blank or null.");
        }
        this.name = name;
    }

    public double getBalance() {
        return this.balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    protected double round(double amount) {
        DecimalFormat formatter = new DecimalFormat("###.##");
        String roundedAmount = formatter.format(amount).replace(",", ".");
        if (Character.isDigit(roundedAmount.charAt(0))) {
            return Double.parseDouble(roundedAmount);
        } else {
            return -1 * Double.parseDouble(roundedAmount.substring(1));
        }
    }

    public abstract void deposit(double amount);
    public abstract boolean withdraw(double amount);
    public abstract Account clone();

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "    " +
                "\t" + this.getId() + "" +
                "\t" + this.getName() + "" +
                "\t$" + this.getBalance() + "";
    }
}