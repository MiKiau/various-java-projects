package src.main.model.account;

import src.main.model.account.impl.Taxable;

public class Chequing extends Account implements Taxable { 
    private static final double OVERDRAFT_LIMIT = 200.;
    private static final double OVERDRAFT_FEE = 5.5;

    private static final double TAXABLE_INCOME = 3000;
    private static final double TAX_RATE = 0.15;

    public Chequing(String id, String name, double balance) {
        super(id, name, balance);
    }

    public Chequing(Chequing source) {
        super(source);
    }

    public static double getOverdraftLimit() {
        return OVERDRAFT_LIMIT;
    }

    public static double getOverdraftFee() {
        return OVERDRAFT_FEE;
    }

    public static double getTaxRate() {
        return TAX_RATE;
    }

    public void deposit(double amount) {
        super.setBalance(super.round(super.getBalance() + amount));
    }

    public boolean withdraw(double amount) {
        boolean result = true;
        if (super.getBalance() - amount + OVERDRAFT_LIMIT <= 0) {
            result = !result;
        } else if (super.getBalance() - amount < 0) {
            super.setBalance(super.round(super.getBalance() - amount - OVERDRAFT_FEE));
        } else {
            super.setBalance(super.round(super.getBalance() - amount));
        }
        return result;
    }

    @Override
    public Account clone() {
        return new Chequing(this);
    }

    @Override
    public void tax(double income) {
        double tax = Math.max(0, income - TAXABLE_INCOME) * TAX_RATE;
        if (tax > 0) {
            super.setBalance(super.round(super.getBalance() - tax));
        }
    }
}
