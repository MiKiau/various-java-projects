package src.main.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

import src.main.model.account.*;
import src.main.model.account.impl.Taxable;

public class Bank {
    private ArrayList<Account> accounts;
    private ArrayList<Transaction> transactions;

    public Bank() {
        accounts = new ArrayList<Account>();
        transactions = new ArrayList<Transaction>();
    }

    public void addAccount(Account account) {
        accounts.add(account.clone());
    }

    private void addTransaction(Transaction transaction) {
        transactions.add(new Transaction(transaction));
    }

    public Transaction[] getTransactions(String accountId) {
        return transactions.stream()
            .filter((transaction) -> transaction.getId().equals(accountId))
            .collect(Collectors.toList())
            .toArray(new Transaction[0]);
    }

    public Account getAccount(String transactionId) {
        return accounts.stream()
            .filter((account) -> account.getId().equals(transactionId))
            .findFirst()
            .orElse(null);
    }

    private double getIncome(Account account) {
        return Arrays.asList(this.getTransactions(account.getId())).stream()
            .map(transaction-> {
                switch (transaction.getType()) {
                    case WITHDRAW: return -transaction.getAmount();
                    case DEPOSIT: return transaction.getAmount();
                    default: return 0.;
                }
            })
            .reduce(0., Double::sum);
    }

    private void withdrawTransaction(Transaction transaction) {
        Account account = this.getAccount(transaction.getId());
        if (account.withdraw(transaction.getAmount())) {
            addTransaction(transaction);
        }
    }

    private void depositTransaction(Transaction transaction) {
        Account account = this.getAccount(transaction.getId());
        account.deposit(transaction.getAmount());
        addTransaction(transaction);
    }

    public void executeTransaction(Transaction transaction) {
        switch (transaction.getType()) {
            case WITHDRAW: withdrawTransaction(transaction); break;
            case DEPOSIT: depositTransaction(transaction); break;
        }
    }

    public void deductTaxes() {
        for (Account account : accounts) {
            if (Taxable.class.isAssignableFrom(account.getClass())) {
                Taxable taxableAccount = (Taxable) account;
                taxableAccount.tax(this.getIncome(account));
            }
        }
    }
}
