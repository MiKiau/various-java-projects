package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;

import main.model.Trade;
import main.model.Trade.Stock;
import main.model.Trade.Type;
import main.model.account.*;

public class BuyTests {
    Account[] accounts;

    @Before
    public void setup() {
        accounts = new Account[] {
            new Personal(500),
            new TFSA(6000)
        };
    }

    @Test
    public void sharesOfStockIncreaseAfterPurchaseInPersonalAccountTest() {
        accounts[0].executeTrade(new Trade(Stock.AAPL, Type.MARKET_BUY, 2, 55.5));
        assertEquals(2, accounts[0].getSharesOf(Stock.AAPL));
    }

    @Test
    public void unsuccessfulBuyingSharesInPersonalAccountTest() {
        accounts[0].executeTrade(new Trade(Stock.AAPL, Type.MARKET_BUY, 50, 55.5));
        assertEquals(0, accounts[0].getSharesOf(Stock.AAPL));
    }

    @Test
    public void fundsAreDeductedAfterSuccessfulBuyingOfSharesInPersonalAccount() {
        accounts[0].executeTrade(new Trade(Stock.AAPL, Type.MARKET_BUY, 2, 50.003));
        assertEquals(399.99, accounts[0].getFunds());
    }

    @Test
    public void sharesOfStockIncreaseAfterPurchaseInTFSAAccountTest() {
        accounts[1].executeTrade(new Trade(Stock.AAPL, Type.MARKET_BUY, 2, 55.5));
        assertEquals(2, accounts[1].getSharesOf(Stock.AAPL));
    }

    @Test
    public void unsuccessfulBuyingSharesInTFSAlAccountTest() {
        accounts[1].executeTrade(new Trade(Stock.AAPL, Type.MARKET_BUY, 1000, 55.5));
        assertEquals(0, accounts[1].getSharesOf(Stock.AAPL));
    }

    @Test
    public void fundsAreDeductedAfterSuccessfulBuyingOfSharesInTFSAAccount() {
        accounts[1].executeTrade(new Trade(Stock.AAPL, Type.MARKET_BUY, 10, 50.003));
        assertEquals(5494.97, accounts[1].getFunds());
    }
}