package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;

import main.model.Trade;
import main.model.Trade.Stock;
import main.model.Trade.Type;
import main.model.account.*;

public class SellTests {
    Account[] accounts;

    @Before
    public void setup() {
        accounts = new Account[] {
            new Personal(500),
            new TFSA(6000)
        };

        accounts[0].executeTrade(new Trade(Stock.AAPL, Type.MARKET_BUY, 5, 15));
        accounts[0].executeTrade(new Trade(Stock.GOOG, Type.MARKET_BUY, 10, 20));
        accounts[1].executeTrade(new Trade(Stock.FB, Type.MARKET_BUY, 50, 40));
        accounts[1].executeTrade(new Trade(Stock.TSLA, Type.MARKET_BUY, 25, 30));
    }

    @Test
    public void successfulSharesSellingDecreaseSharesAmountInPersonalAccountTest() {
        accounts[0].executeTrade(new Trade(Stock.AAPL, Type.MARKET_SELL, 3, 20));
        assertEquals(2, accounts[0].getSharesOf(Stock.AAPL));
    }

    @Test
    public void unsuccessfulSharesSellingDoesNotDecreaseSharesInPersonalAccountTest() {
        accounts[0].executeTrade(new Trade(Stock.AAPL, Type.MARKET_SELL, 6, 20));
        assertEquals(5, accounts[0].getSharesOf(Stock.AAPL));
    }

    @Test
    public void unsuccessfulSharesSellingDoesNotIncreaseFundsInPersonalAccountTest() {
        accounts[0].executeTrade(new Trade(Stock.AAPL, Type.MARKET_SELL, 6, 20));
        assertEquals(225, accounts[0].getFunds());
    }

    @Test
    public void successfulSharesSellingIncreasesFundsInPersonalAccountTest() {
        accounts[0].executeTrade(new Trade(Stock.AAPL, Type.MARKET_SELL, 5, 20.003));
        assertEquals(320.01, accounts[0].getFunds());
    }

    @Test
    public void successfulSharesSellingDecreaseSharesAmountInTFSAAccountTest() {
        accounts[1].executeTrade(new Trade(Stock.FB, Type.MARKET_SELL, 3, 20));
        assertEquals(47, accounts[1].getSharesOf(Stock.FB));
    }

    @Test
    public void unsuccessfulSharesSellingDoesNotDecreaseSharesInPersonalTSFATest() {
        accounts[1].executeTrade(new Trade(Stock.FB, Type.MARKET_SELL, 51, 20));
        assertEquals(50, accounts[1].getSharesOf(Stock.FB));
    }

    @Test
    public void unsuccessfulSharesSellingDoesNotIncreaseFundsInTSFAAccountTest() {
        accounts[1].executeTrade(new Trade(Stock.FB, Type.MARKET_SELL, 51, 20));
        assertEquals(3222.5, accounts[1].getFunds());
    }

    @Test
    public void successfulSharesSellingIncreasesFundsInTSFAAccountTest() {
        accounts[1].executeTrade(new Trade(Stock.FB, Type.MARKET_SELL, 5, 20.003));
        assertEquals(3321.51, accounts[1].getFunds());
    }
}