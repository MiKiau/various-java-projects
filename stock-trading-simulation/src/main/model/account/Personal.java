package main.model.account;

import main.model.Trade.Stock;

public class Personal extends Account {
    private static final double SALE_FEE = 0.05;
    private static final double BUYING_FEE = 0.00;

    public Personal(double funds) {
        super(funds);
    }

    public Personal(Personal source) {
        super(source);
    }

    public static double getSaleFee() {
        return SALE_FEE;
    }

    public static double getBuyingFee() {
        return BUYING_FEE;
    }

    @Override
    protected boolean sellShares(Stock stock, int amount, double price) {
        return super.sellSharesWithTax(stock, amount, price, getSaleFee());
    }

    @Override
    protected boolean buyShares(Stock stock, int amount, double price) {
        return super.buySharesWithTax(stock, amount, price, getBuyingFee());
    }
}
