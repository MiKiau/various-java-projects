package main.model.account;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

import main.model.Trade;
import main.model.Trade.Stock;
import main.utils.Color;

public abstract class Account {
    private HashMap<Stock, Integer> portofolio;
    private double funds;

    public Account(double funds) throws IllegalArgumentException {
        portofolio = new HashMap<Stock, Integer>();
        for (Stock stock : Stock.values()) {
            portofolio.put(stock, 0);
        }
        setFunds(funds);
    }

    public Account(Account source) {
        for (Stock stock : Stock.values()) {
            this.setSharesTo(stock, source.getSharesOf(stock));
        }
        this.setFunds(source.getFunds());
    }

    public int getSharesOf(Stock stock) {
        return this.portofolio.get(stock);
    }

    private void setSharesTo(Stock stock, int amount) {
        this.portofolio.put(stock, amount);
    }

    public double getFunds() {
        return this.funds;
    }

    private void setFunds(double funds) throws IllegalArgumentException {
        if (funds < 0) {
            throw new IllegalArgumentException("Account funds cannot be negative.");
        }
        this.funds = funds;
    }

    protected boolean sellSharesWithTax(Stock stock, int amount, double price, double tax) {
        if (this.getSharesOf(stock) - amount >= 0) {
            this.setSharesTo(stock, this.getSharesOf(stock) - amount);
            this.setFunds(this.round(this.getFunds() + amount * price * (1 - tax)));
            return true;
        }
        return false;
    }

    protected boolean buySharesWithTax(Stock stock, int amount, double price, double tax) {
        if (this.getFunds() - amount * price >= 0) {
            this.setSharesTo(stock, this.getSharesOf(stock) + amount);
            this.setFunds(this.round(this.getFunds() - amount * price * (1 + tax)));
            return true;
        }
        return false;
    }

    protected abstract boolean sellShares(Stock stock, int amount, double price);
    protected abstract boolean buyShares(Stock stock, int amount, double price);

    public boolean executeTrade(Trade trade) {
        switch (trade.getType()) {
            case MARKET_SELL: return sellShares(trade.getStock(), trade.getAmount(), trade.getPrice());
            case MARKET_BUY:  return buyShares(trade.getStock(), trade.getAmount(), trade.getPrice());
            default:          return false;
        }
    }
    
    private double round(double amount) {
        DecimalFormat formatter = new DecimalFormat("###.##");
        String formatedNumber = formatter.format(amount).replace(",", ".");
        if (Character.isDigit(formatedNumber.charAt(0))) {
            return Double.parseDouble(formatedNumber);
        } else {
            return -Double.parseDouble(formatedNumber.substring(1));
        }
    }
    
    private String displayPortofolio() {
        return Arrays.asList(Stock.values()).stream()
            .map((stock) -> ("  " + Color.BLUE + stock.toString() + "\t\t" 
                            + Color.GREEN + portofolio.get(stock) + "\n"))
            .reduce("", (str1, str2) -> str1 + str2);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Account)) {
            return false;
        }
        Account account = (Account) o;
        return Objects.equals(portofolio, account.portofolio) && funds == account.funds;
    }

    @Override
    public int hashCode() {
        return Objects.hash(portofolio, funds);
    }

    @Override
    public String toString() {
        return "\n  Stock\t\t"  + Color.RESET + "Shares" +
        "\n\n" + displayPortofolio() + Color.RESET +
        "\n  Funds Left\t" + Color.GREEN + "$" + Math.round(this.getFunds()) + Color.RESET;
    }
}
