package main.model.account;

import main.model.Trade.Stock;;

public class TFSA extends Account {
    private static final double TRADE_FEE = 0.01;

    public TFSA(double funds) {
        super(funds);
    }

    public TFSA(TFSA source) {
        super(source);
    }

    public static double getTradeFee() {
        return TRADE_FEE;
    }

    @Override
    protected boolean sellShares(Stock stock, int amount, double price) {
        return super.sellSharesWithTax(stock, amount, price, getTradeFee());
    }

    @Override
    protected boolean buyShares(Stock stock, int amount, double price) {
        return super.buySharesWithTax(stock, amount, price, getTradeFee());
    }
}
