package main.model;

import java.util.Objects;

public class Trade {
    public enum Stock {AAPL, FB, GOOG, TSLA};
    public enum Type {MARKET_BUY, MARKET_SELL};

    private Stock stock;
    private Type type;
    private int amount;
    private double price;

    public Trade(Stock stock, Type type, int amount, 
            double price) throws IllegalArgumentException {
        this.setStock(stock);
        this.setType(type);
        this.setAmount(amount);
        this.setPrice(price);
    }

    public Trade(Trade source) {
        this.setStock(source.getStock());
        this.setType(source.getType());
        this.setAmount(source.getAmount());
        this.setPrice(source.getPrice());
    }

    public Stock getStock() {
        return this.stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) throws IllegalArgumentException {
        if (amount < 0) {
            throw new IllegalArgumentException("Trade amount cannot be negative.");
        }
        this.amount = amount;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) throws IllegalArgumentException {
        if (price < 0) {
            throw new IllegalArgumentException("Trade price cannot be negative.");
        }
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Trade)) {
            return false;
        }
        Trade trade = (Trade) o;
        return Objects.equals(stock, trade.stock) && Objects.equals(type, trade.type) && amount == trade.amount && price == trade.price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(stock, type, amount, price);
    }

    @Override
    public String toString() {
        return "  " + this.getStock().toString() + "\t"
            + this.getType().toString() + "\t$"
            + this.getPrice() + "\t"
            + this.getAmount();
    }
}